#!/bin/bash

# This is a minor shell script to ensure vpn connection when the wifi interface comes up.
# Using this script in the pre-up phase and setting up a firewall afterwards will prevent
# leaks before it comes up and in case it goes down. You will need to change some of these
# variables if you want to use this as it refers to my Network Manager vpn and my 
# network interfaces.

# Also, don't use PIA. Use mullvad.

NO_VPN_INTERFACES=( "lo", "tun0" )
VPN_NAME="PIA"

SHOULD_BUILDWALL=true # Security by default.
EVENT_INTERFACE=$0

echo "$EVENT_INTERFACE" triggered if-pre-up VPN Firewall script.

for element in ${NETWORK_INTERFACES[@a]}	
do
        if [ $EVENT_INTERFACE == $element ]
	then
		echo "$EVENT_INTERFACE" loaded on blacklist of interfaces which need no vpn connection, exiting.
                exit 0
	fi
done

echo Enabling firewall and connecting to VPN.
nmcli con up id "$VPN_NAME"
# TODO: LOAD NFT RULESET
